import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { SearchPage } from '../search/search';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {

  nav;
  theSearchPage = SearchPage;

  constructor(public navCtrl: NavController) {
    this.nav = navCtrl;
  }
  goToSearch(){
    this.nav.setRoot(SearchPage);
  }

}
