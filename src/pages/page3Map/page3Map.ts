import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
// import { HomePage } from '../pages/home/home';

@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
})
export class Page3Map {

  // rootPage: any = HomePage;

  constructor(platform: Platform) {

    platform.ready().then(() => {
      Geolocation.getCurrentPosition().then((resp) => {
        console.log("Latitude: ", resp.coords.latitude);
        console.log("Longitude: ", resp.coords.longitude);
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });

    let watch = Geolocation.watchPosition();
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
      console.log("Latitude: ", data.coords.latitude);
      console.log("Longitude: ", data.coords.longitude);
    });
  }



}