
##### Let’s use the Geolocation plugin as an example. You will need to install the plugin you want to use. 

`ionic plugin add cordova-plugin-geolocation`

####  Add android platform

`ionic platform add android`

#### Add search page
`ionic generate page SearchPage`